const myModal = new bootstrap.Modal("#transaction-modal");
let logged = sessionStorage.getItem("logged");
const session = localStorage.getItem("session");
let data = {
    transactions: []
};

document.getElementById("btn-logout").addEventListener("click", logout);

// Adicionar Lançamento
document.getElementById("transaction-form").addEventListener("submit", function(e){
    e.preventDefault();

    const value = parseFloat(document.getElementById("value-input").value);
    const description = document.getElementById("description-input").value;
    const date = document.getElementById("date-input").value;
    const type = document.querySelector('input[name="type-input"]:checked').value;

    data.transactions.unshift({
        value: value,
        type: type,
        description: description,
        date: date
    });

    saveData(data);
    e.target.reset();
    myModal.hide();

    alert("Lançamento efetuado");
})

checkLogged();

// Checar se está logado e quem
function checkLogged(){
    if(session){
        sessionStorage.setItem("logged", session);
        logged = session;
    }

    if(!logged){
        window.location.href = "index.html";
        return;
    }

    const dataUser = localStorage.getItem(logged)
    if(dataUser){
        data = JSON.parse(dataUser);
    }

    getCashIn();
}

// Logout
function logout(){
    sessionStorage.removeItem("logged");
    localStorage.removeItem("session");
    window.location.href = "index.html";
}

// Salvar dados
function saveData(data){
    localStorage.setItem(data.login, JSON.stringify(data));
}

// Mostrar Entradas
function getCashIn(){
    const transactions = data.transactions;
    
    const cashIn = transactions.filter((item) => item.type === "1");

    if(cashIn.length){
        let cashInHTML = ``;
        let limit = 0;
        if(cashIn.length > 5){
            limit = 5;
        }else{
            limit = cashIn.length;
        }
        
        for(let index = 0; index < limit; index++){
            cashInHTML += `${cashIn[index]}`;
        }
    
    }

}

// Mostrar Saídas
function getCashOut(){
    const transactions = data.transactions;
    
    const cashIn = transactions.filter((item) => item.type === "2");
}